//
//  ViewController.swift
//  HW5
//
//  Created by erjigit on 1/13/20.
//  Copyright © 2020 erjigit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func goPress(_ sender: Any) {
        
        print(userNameTextField.text)
        if userNameTextField.text == "u" && passwordTextField.text == "p" {
            performSegue(withIdentifier: "goSegue", sender: nil)
        } else {
            let alertController = UIAlertController(title: "Attention", message: "Wrong User Name or Password", preferredStyle: .alert)
            let alertOKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(alertOKAction)

            present(alertController, animated: true, completion: nil)
        }

        
        
    }
    
    @IBAction func nameForgot(_ sender: Any) {
        let nameAlertController = UIAlertController(title: "Hint", message: "Name is u", preferredStyle: .alert)
        let nameAlertOKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        nameAlertController.addAction(nameAlertOKAction)

        present(nameAlertController, animated: true, completion: nil)
        
    }
    @IBAction func passwordForgot(_ sender: Any) {
        let passworAlertController = UIAlertController(title: "Hint", message: "Password is p", preferredStyle: .alert)
        let passworAlertOKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        passworAlertController.addAction(passworAlertOKAction)

        present(passworAlertController, animated: true, completion: nil)
    }
    
}




